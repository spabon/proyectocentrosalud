package proyectoCentroSalud.model.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the suple database table.
 * 
 */
@Entity
@Table(name="suple")
@NamedQuery(name="Suple.findAll", query="SELECT s FROM Suple s")
public class Suple implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SUPLE_IDSUPLE_GENERATOR", sequenceName="public.suple_id_suple_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SUPLE_IDSUPLE_GENERATOR")
	@Column(name="id_suple", unique=true, nullable=false)
	private Integer idSuple;

	@Column(length=200)
	private String descripcion;

	public Suple() {
	}

	public Integer getIdSuple() {
		return this.idSuple;
	}

	public void setIdSuple(Integer idSuple) {
		this.idSuple = idSuple;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}