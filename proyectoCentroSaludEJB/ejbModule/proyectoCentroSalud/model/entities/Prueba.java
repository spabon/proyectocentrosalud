package proyectoCentroSalud.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the prueba database table.
 * 
 */
@Entity
@Table(name="prueba")
@NamedQuery(name="Prueba.findAll", query="SELECT p FROM Prueba p")
public class Prueba implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PRUEBA_IDPRUEBA_GENERATOR", sequenceName="public.seq_prueba",allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PRUEBA_IDPRUEBA_GENERATOR")
	@Column(unique=true, nullable=false)
	private Integer idprueba;

	private Integer cantidad;

	@Column(length=100)
	private String descripcion;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	@Column(precision=8, scale=2)
	private BigDecimal precio;

	public Prueba() {
	}

	public Integer getIdprueba() {
		return this.idprueba;
	}

	public void setIdprueba(Integer idprueba) {
		this.idprueba = idprueba;
	}

	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public BigDecimal getPrecio() {
		return this.precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

}