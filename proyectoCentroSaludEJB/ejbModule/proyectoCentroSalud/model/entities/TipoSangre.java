package proyectoCentroSalud.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tipo_sangre database table.
 * 
 */
@Entity
@Table(name="tipo_sangre")
@NamedQuery(name="TipoSangre.findAll", query="SELECT t FROM TipoSangre t")
public class TipoSangre implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TIPO_SANGRE_IDTIPOSANGRE_GENERATOR", sequenceName="public.tipo_sangre_id_tipo_sangre_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TIPO_SANGRE_IDTIPOSANGRE_GENERATOR")
	@Column(name="id_tipo_sangre", unique=true, nullable=false)
	private Integer idTipoSangre;

	@Column(name="grupo_sanguineo", nullable=false, length=4)
	private String grupoSanguineo;

	//bi-directional many-to-one association to Paciente
	@OneToMany(mappedBy="tipoSangre")
	private List<Paciente> pacientes;

	public TipoSangre() {
	}

	public Integer getIdTipoSangre() {
		return this.idTipoSangre;
	}

	public void setIdTipoSangre(Integer idTipoSangre) {
		this.idTipoSangre = idTipoSangre;
	}

	public String getGrupoSanguineo() {
		return this.grupoSanguineo;
	}

	public void setGrupoSanguineo(String grupoSanguineo) {
		this.grupoSanguineo = grupoSanguineo;
	}

	public List<Paciente> getPacientes() {
		return this.pacientes;
	}

	public void setPacientes(List<Paciente> pacientes) {
		this.pacientes = pacientes;
	}

	public Paciente addPaciente(Paciente paciente) {
		getPacientes().add(paciente);
		paciente.setTipoSangre(this);

		return paciente;
	}

	public Paciente removePaciente(Paciente paciente) {
		getPacientes().remove(paciente);
		paciente.setTipoSangre(null);

		return paciente;
	}

}