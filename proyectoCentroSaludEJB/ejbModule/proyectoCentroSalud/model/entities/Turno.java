package proyectoCentroSalud.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;
import java.util.Date;


/**
 * The persistent class for the turno database table.
 * 
 */
@Entity
@Table(name="turno")
@NamedQuery(name="Turno.findAll", query="SELECT t FROM Turno t")
public class Turno implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TURNO_IDTURNO_GENERATOR", sequenceName="public.turno_id_turno_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TURNO_IDTURNO_GENERATOR")
	@Column(name="id_turno", unique=true, nullable=false)
	private Integer idTurno;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_reserva", nullable=false)
	private Date fechaReserva;

	@Column(nullable=false)
	private String hora;

	@Column(name="numero_reserva", nullable=false)
	private Integer numeroReserva;

	//bi-directional many-to-one association to Area
	@ManyToOne
	@JoinColumn(name="id_area", nullable=false)
	private Area area;

	//bi-directional many-to-one association to Medico
	@ManyToOne
	@JoinColumn(name="id_medico", nullable=false)
	private Medico medico;

	//bi-directional many-to-one association to Paciente
	@ManyToOne
	@JoinColumn(name="id_paciente", nullable=false)
	private Paciente paciente;

	public Turno() {
	}

	public Integer getIdTurno() {
		return this.idTurno;
	}

	public void setIdTurno(Integer idTurno) {
		this.idTurno = idTurno;
	}

	public Date getFechaReserva() {
		return this.fechaReserva;
	}

	public void setFechaReserva(Date fechaReserva) {
		this.fechaReserva = fechaReserva;
	}

	public String getHora() {
		return this.hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public Integer getNumeroReserva() {
		return this.numeroReserva;
	}

	public void setNumeroReserva(Integer numeroReserva) {
		this.numeroReserva = numeroReserva;
	}

	public Area getArea() {
		return this.area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Medico getMedico() {
		return this.medico;
	}

	public void setMedico(Medico medico) {
		this.medico = medico;
	}

	public Paciente getPaciente() {
		return this.paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

}