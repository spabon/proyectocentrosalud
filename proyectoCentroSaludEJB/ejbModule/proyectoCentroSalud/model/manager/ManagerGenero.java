package proyectoCentroSalud.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import proyectoCentroSalud.model.entities.Genero;;

/**
 * Session Bean implementation class ManagerGenero
 */
@Stateless
@LocalBean
public class ManagerGenero {

	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public ManagerGenero() {
        // TODO Auto-generated constructor stub
    }
    
    //LISTAR TABLA
    public List<Genero> findAll(){   	
    	return em.createNamedQuery("Genero.findAll", Genero.class).getResultList();
    }
    //BUSCAR
    public Genero findById(int id_genero) {
    	return em.find(Genero.class, id_genero);
    }
    //INSERTAR
    public void insertar(Genero genero) throws Exception {
  		em.persist(genero);
  	}
    //ELIMINAR
    public void eliminar(int id_genero) {
    	Genero genero = findById(id_genero);
    	if(genero!=null)
    		em.remove(genero);
    }
    //ACTUALIZAR
    public void actualizar(Genero genero) throws Exception{
    	Genero r= findById(genero.getIdGenero());
    	if(r==null)
    		throw new Exception("No existe el genero con el código especificado");
    	r.setNombre(genero.getNombre());
    	em.merge(r);
    }

}
