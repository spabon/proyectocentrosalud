package proyectoCentroSalud.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import proyectoCentroSalud.model.entities.Ciudad;

/**
 * Session Bean implementation class ManagerCiudad
 */
@Stateless
@LocalBean
public class ManagerCiudad {
	
	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public ManagerCiudad() {
        // TODO Auto-generated constructor stub
    }

  //LISTAR TABLA
    public List<Ciudad> findAllCiudad(){   	
    	return em.createNamedQuery("Ciudad.findAll", Ciudad.class).getResultList();
    }
    //BUSCAR
    public Ciudad findCiudadById(int id_ciudad) {
    	return em.find(Ciudad.class, id_ciudad);
    }
    //INSERTAR
    public void insertarCiudad(Ciudad ciudad) throws Exception {
  		em.persist(ciudad);
  	}
    //ELIMINAR
    public void eliminarCiudad(int id_ciudad) {
    	Ciudad ciudad = findCiudadById(id_ciudad);
    	if(ciudad!=null)
    		em.remove(ciudad);
    }
    //ACTUALIZAR
    public void actualizarCiudad(Ciudad ciudad) throws Exception{
    	Ciudad r= findCiudadById(ciudad.getIdCiudad());
    	if(r==null)
    		throw new Exception("No existe el area con el código especificado");
    	r.setNombre(ciudad.getNombre());
    	em.merge(r);
    }
}
