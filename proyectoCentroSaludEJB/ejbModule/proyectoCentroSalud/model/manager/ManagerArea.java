package proyectoCentroSalud.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import proyectoCentroSalud.model.entities.Area;;

/**
 * Session Bean implementation class ManagerArea
 */
@Stateless
@LocalBean
public class ManagerArea {
	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public ManagerArea() {
        // TODO Auto-generated constructor stub
    }
   //LISTAR TABLA
    public List<Area> findAllArea(){   	
    	return em.createNamedQuery("Area.findAll", Area.class).getResultList();
    }
    //BUSCAR
    public Area findAreaById(int id_area) {
    	return em.find(Area.class, id_area);
    }
    //INSERTAR
    public void insertarArea(Area area) throws Exception {
  		em.persist(area);
  	}
    //ELIMINAR
    public void eliminarArea(int id_area) {
    	Area area = findAreaById(id_area);
    	if(area!=null)
    		em.remove(area);
    }
    //ACTUALIZAR
    public void actualizarArea(Area area) throws Exception{
    	Area r= findAreaById(area.getIdArea());
    	if(r==null)
    		throw new Exception("No existe el area con el código especificado");
    	r.setDescripcion(area.getDescripcion());
    	em.merge(r);
    }

}
