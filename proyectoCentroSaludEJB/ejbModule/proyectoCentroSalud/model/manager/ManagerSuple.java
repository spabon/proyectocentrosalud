package proyectoCentroSalud.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import proyectoCentroSalud.model.entities.Suple;

/**
 * Session Bean implementation class ManagerSuple
 */
@Stateless
@LocalBean
public class ManagerSuple {

	@PersistenceContext
	private EntityManager em;

    public ManagerSuple() {
        // TODO Auto-generated constructor stub
    }
    //Listar Tabla
    public List<Suple> findAllSuple (){
    	return em.createNamedQuery("Suple.findAll", Suple.class).getResultList();
    }
    //Buscar
    public Suple findSupleById(int id_suple) {
    	return em.find(Suple.class, id_suple) ;
    	}
    //insertar
    public void insertarSuple(Suple suple) throws Exception {
    	em.persist(suple);    	
    }
    //eliminar
    public void eliminarSuple(int id_suple) {
    	Suple suple = findSupleById(id_suple);
    	if (suple!=null)
    		em.remove(suple);
    	
    }
    ///actualizar
    public void actualizarSuple(Suple suple) throws Exception{
    	Suple s= findSupleById(suple.getIdSuple());
    	if (s==null)
    		throw new Exception("No existe registro o codigo..... ");
    	s.setDescripcion(suple.getDescripcion());
    	em.merge(s);
    }
    

}
