package proyectoCentroSalud.model.manager;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import proyectoCentroSalud.model.entities.Cargo;
import proyectoCentroSalud.model.entities.Ciudad;
import proyectoCentroSalud.model.entities.Genero;
import proyectoCentroSalud.model.entities.Medico;



/**
 * Session Bean implementation class ManagerMedico
 */
@Stateless
@LocalBean
public class ManagerMedico {
	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
	@EJB 
	private ManagerMedico mMedico;
    public ManagerMedico() {
        // TODO Auto-generated constructor stub
    }
    
    public List<Medico> findAllMedico(){
    	return em.createNamedQuery("Medico.findAll", Medico.class).getResultList();
    }
    
    public List<Cargo> findAllCargo(){
    	return em.createNamedQuery("Cargo.findAll", Cargo.class).getResultList();
    }
    
    public List<Genero> findAllGenero(){
    	return em.createNamedQuery("Genero.findAll", Genero.class).getResultList();
    }
    
    public List<Ciudad> findAllCiudad(){
    	return em.createNamedQuery("Ciudad.findAll", Ciudad.class).getResultList();
    }
    
    
    public Medico findMedicobyId(int id) {
    	return em.find(Medico.class, id);
    }
    
    public Cargo findCargobyId(int id) {
    	return em.find(Cargo.class, id);
    }
    
    public Genero findGenerobyId(int id) {
    	return em.find(Genero.class, id);
    }
    
    public Ciudad findCiudadbyId(int id) {
    	return em.find(Ciudad.class, id);
    }
    
    public void insertarMedico(Medico medico, int codigo_cargo,int codigo_genero, int codigo_ciudad) {
    	Cargo cargo;
    	Genero genero;
    	Ciudad ciudad;
    	cargo = mMedico.findCargobyId(codigo_cargo);
    	genero = mMedico.findGenerobyId(codigo_genero);
    	ciudad = mMedico.findCiudadbyId(codigo_ciudad);
    	
    	medico.setCargo(cargo);
    	medico.setGenero(genero);
    	medico.setCiudad(ciudad);
    	
    	em.persist(medico);
    }
    
}
