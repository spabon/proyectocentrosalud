package proyectoCentroSalud.model.manager;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import proyectoCentroSalud.model.entities.Area;
import proyectoCentroSalud.model.entities.Medico;
import proyectoCentroSalud.model.entities.Paciente;
import proyectoCentroSalud.model.entities.Turno;

/**
 * Session Bean implementation class ManagerTurno
 */
@Stateless
@LocalBean
public class ManagerTurno {
	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
	@EJB
	private ManagerTurno mTurno;
    public ManagerTurno() {
        // TODO Auto-generated constructor stub
    }
    
    public List<Turno> findAllTurno(){
    	return em.createNamedQuery("Turno.findAll", Turno.class).getResultList();
    }
    
    public List<Paciente> findAllPaciente(){
    	return em.createNamedQuery("Paciente.findAll", Paciente.class).getResultList();
    }
    
    public List<Medico> findAllMedico(){
    	return em.createNamedQuery("Medico.findAll", Medico.class).getResultList();
    }
    
    public List<Area> findAllArea(){
    	return em.createNamedQuery("Area.findAll", Area.class).getResultList();
    }
    
    
    
    
    public Turno findTurnobyId(int id) {
    	return em.find(Turno.class, id);
    }
    
    
    public Paciente findPacientebyId(int id) {
    	return em.find(Paciente.class, id);
    }
    
    public Medico findMedicobyId(int id) {
    	return em.find(Medico.class, id);
    }
    
    public Area findAreaId(int id) {
    	return em.find(Area.class, id);
    }
    
    public void insertarTurno(Turno turno, int codigo_paciente, int codigo_medico, int codigo_area) {
    	Medico medico;
    	Paciente paciente;
    	Area area;
    	medico = mTurno.findMedicobyId(codigo_medico);
    	paciente= mTurno.findPacientebyId(codigo_paciente);
    	area = mTurno.findAreaId(codigo_area);
    	
    	turno.setMedico(medico);
    	turno.setPaciente(paciente);
    	turno.setArea(area);
    	em.persist(turno);
    }

}
