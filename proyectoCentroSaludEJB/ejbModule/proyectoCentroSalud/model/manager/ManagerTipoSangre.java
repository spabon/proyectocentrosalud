package proyectoCentroSalud.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import proyectoCentroSalud.model.entities.TipoSangre;;

/**
 * Session Bean implementation class ManagerTipoSangre
 */
@Stateless
@LocalBean
public class ManagerTipoSangre {

	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public ManagerTipoSangre() {
        // TODO Auto-generated constructor stub
    }


    //LISTAR TABLA
      public List<TipoSangre> findAllTipoSangre(){   	
      	return em.createNamedQuery("TipoSangre.findAll", TipoSangre.class).getResultList();
      }
      //BUSCAR
      public TipoSangre findTipoSangreById(int id_tipoSangre) {
      	return em.find(TipoSangre.class, id_tipoSangre);
      }
      //INSERTAR
      public void insertarTipoSangre(TipoSangre tipoSangre) throws Exception {
    	  em.persist(tipoSangre);
    	}
      //ELIMINAR
      public void eliminarTipoSangre(int id_tipoSangre) {
    	  TipoSangre tipoSangre = findTipoSangreById(id_tipoSangre);
      	if(tipoSangre!=null)
      		em.remove(tipoSangre);
      }
      //ACTUALIZAR
      public void actualizarTipoSangre(TipoSangre tipoSangre) throws Exception{
    	  TipoSangre r= findTipoSangreById(tipoSangre.getIdTipoSangre());
      	if(r==null)
      		throw new Exception("No existe el area con el código especificado");
      	r.setGrupoSanguineo(tipoSangre.getGrupoSanguineo());
      	em.merge(r);
      }
}
