package proyectoCentroSalud.model.manager;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


import proyectoCentroSalud.model.entities.Ciudad;
import proyectoCentroSalud.model.entities.Genero;

import proyectoCentroSalud.model.entities.Paciente;
import proyectoCentroSalud.model.entities.TipoSangre;

/**
 * Session Bean implementation class ManagerPaciente
 */
@Stateless
@LocalBean
public class ManagerPaciente {
	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
	@EJB 
	private ManagerPaciente mPaciente;
    public ManagerPaciente() {
        // TODO Auto-generated constructor stub
    }
    
    public List<Paciente> findAllPaciente(){
    	return em.createNamedQuery("Paciente.findAll", Paciente.class).getResultList();
    }
    
    public List<Ciudad> findAllCiudad(){
    	return em.createNamedQuery("Ciudad.findAll", Ciudad.class).getResultList();
    }
    
    public List<Genero> findAllGenero(){
    	return em.createNamedQuery("Genero.findAll", Genero.class).getResultList();
    }
    
    public List<TipoSangre> findAllTipoSangre(){
    	return em.createNamedQuery("TipoSangre.findAll", TipoSangre.class).getResultList();
    }
    
    
    
    
    public Paciente findPacientebyId(int id) {
    	return em.find(Paciente.class, id);
    }
    
    
    public Genero findGenerobyId(int id) {
    	return em.find(Genero.class, id);
    }
    
    public Ciudad findCiudadbyId(int id) {
    	return em.find(Ciudad.class, id);
    }
    
    public TipoSangre findTiposangreId(int id) {
    	return em.find(TipoSangre.class, id);
    }
    
    public void insertarPaciente(Paciente paciente, int codigo_genero, int codigo_ciudad, int codigo_tiposangre) {
    	TipoSangre tsangre;
    	Genero genero;
    	Ciudad ciudad;
    	tsangre = mPaciente.findTiposangreId(codigo_tiposangre);
    	genero = mPaciente.findGenerobyId(codigo_genero);
    	ciudad = mPaciente.findCiudadbyId(codigo_ciudad);
    	
    	paciente.setTipoSangre(tsangre);
    	paciente.setGenero(genero);
    	paciente.setCiudad(ciudad);
    	
    	em.persist(paciente);
    }

}
