package proyectoCentroSalud.model.manager;


import java.math.BigDecimal;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


import proyectoCentroSalud.model.entities.Prueba;

/**
 * Session Bean implementation class ManagerPrueba
 */
@Stateless
@LocalBean
public class ManagerPrueba {
	@PersistenceContext
	private EntityManager em;
	
	
    /**
     * Default constructor. 
     */
    public ManagerPrueba() {
        // TODO Auto-generated constructor stub
    }
    //listar tabla
    public List<Prueba> findAllPrueba(){
    	return em.createNamedQuery("Prueba.findAll", Prueba.class).getResultList();
    }
    //buscar
    public Prueba findPruebaByid(int idprueba) {
    	return em.find(Prueba.class, idprueba);
    }
    
    //eliminar
    public void eliminarPrueba(int idprueba) {
    	Prueba prueba = findPruebaByid(idprueba);
    	if(prueba!=null)
    		em.remove(prueba);
    }

    //insertar
    
    public void insertarPrueba (Prueba prueba) throws Exception { 
    	Prueba p=new Prueba(); 
    	p.setDescripcion(prueba.getDescripcion());
    	p.setFecha(prueba.getFecha());
    	p.setCantidad(prueba.getCantidad()); 
    	p.setPrecio(prueba.getPrecio());

    	em.persist(p);
    	
    }
	//metodo suma
	public int totalS (List<Prueba> pruebas) {
		int sum=0;
		for(Prueba p: pruebas)
			sum=sum+p.getCantidad();
		return sum;
	}
//  public double totalSumaP(List<Prueba> pruebas) {
//	   double sum =0;
//	  for(Prueba p: pruebas)
//		sum+=p.getPrecio(new BigDecimal(sum));
//		return sum;
	   
//  }
}
