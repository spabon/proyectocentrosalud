package proyectoCentroSalud.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import proyectoCentroSalud.model.entities.Cargo;;

/**
 * Session Bean implementation class ManagerCargo
 */
@Stateless
@LocalBean
public class ManagerCargo {

	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public ManagerCargo() {
        // TODO Auto-generated constructor stub
    }
    
  //LISTAR TABLA
    public List<Cargo> findAllCargo(){   	
    	return em.createNamedQuery("Cargo.findAll", Cargo.class).getResultList();
    }
    //BUSCAR
    public Cargo findCargoById(int id_cargo) {
    	return em.find(Cargo.class, id_cargo);
    }
    //INSERTAR
    public void insertarCargo(Cargo cargo) throws Exception {
  		em.persist(cargo);
  	}
    //ELIMINAR
    public void eliminarCargo(int id_cargo) {
    	Cargo cargo = findCargoById(id_cargo);
    	if(cargo!=null)
    		em.remove(cargo);
    }
    //ACTUALIZAR
    public void actualizarCargo(Cargo cargo) throws Exception{
    	Cargo r= findCargoById(cargo.getIdCargo());
    	if(r==null)
    		throw new Exception("No existe el area con el código especificado");
    	r.setEspecializacion(cargo.getEspecializacion());
    	em.merge(r);
    }

}
