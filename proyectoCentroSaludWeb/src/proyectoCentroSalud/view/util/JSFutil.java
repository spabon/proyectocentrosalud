package proyectoCentroSalud.view.util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class JSFutil {

	public static void crearMensajeInfo(String mensaje) {
		FacesMessage msg = new FacesMessage();
		msg.setSeverity(FacesMessage.SEVERITY_INFO);
		msg.setSummary(mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public static void crearMensajeWarning(String mensaje) {
		FacesMessage msg = new FacesMessage();
		msg.setSeverity(FacesMessage.SEVERITY_WARN);
		msg.setSummary(mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public static void crearMensajeError(String mensaje) {
		FacesMessage msg = new FacesMessage();
		msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		msg.setSummary(mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}
	
	private int number6;

	public int getNumber6() {
		return number6;
	}

	public void setNumber6(int number6) {
		this.number6 = number6;
	}
	
	
	
}
