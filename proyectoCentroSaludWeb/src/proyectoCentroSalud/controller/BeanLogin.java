package proyectoCentroSalud.controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;



@Named
@SessionScoped
public class BeanLogin implements Serializable {

	private static final long serialVersionUID = 1L;
	private String usuario;
	private String clave;
	private String tipoUsuario;
	
	
	public String login() {
		if(usuario.equals("admin") && clave.equals("qwerty123")) {
			return "Administrador/index.xhtml";
			}
		else if(usuario.equals("usuario") && clave.equals("12345")) {
			return "Usuario/turno.xhtml";
		}
		else if(usuario.equals("empleado") && clave.equals("qwerty")) {
			return "Empleado/index.xhtml";
		}
		else 
			FacesContext.getCurrentInstance().addMessage("", new FacesMessage("Ingreso invalido"));
		return "";
	}
	
	public String cerrarSesion() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/Login?faces-redirect=true";
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
	

}
