package proyectoCentroSalud.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import proyectoCentroSalud.model.entities.TipoSangre;
import proyectoCentroSalud.model.manager.ManagerTipoSangre;
import proyectoCentroSalud.view.util.JSFutil;

@Named
@SessionScoped
public class BeanTipoSangre implements Serializable{
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerTipoSangre m;
	private List<TipoSangre> lista;
	private TipoSangre tipoSangre;
	private boolean panelColapsado;
	private TipoSangre Seleccionado;
	
	@PostConstruct
	public void inicializar() {
		panelColapsado = true;
		tipoSangre = new TipoSangre();
		lista=m.findAllTipoSangre();		
	}
	
	public void actionListenerColapsarPanelTipoSangre() {
		panelColapsado = !panelColapsado;
	}

	public void actionListenerInsertar() {
		try {
			System.out.print(tipoSangre.getGrupoSanguineo());
			m.insertarTipoSangre(tipoSangre);
			lista = m.findAllTipoSangre();
			tipoSangre = new TipoSangre();
			JSFutil.crearMensajeInfo("Dato ingresado");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFutil.crearMensajeError("Dato Duplicado");
			e.printStackTrace();
		}
	}
	
	public void actionListenerEliminar(int id_tipoSangre) {
		m.eliminarTipoSangre(id_tipoSangre);
		lista = m.findAllTipoSangre();
		JSFutil.crearMensajeInfo("Dato eliminado");
	}

	public void actionListenerSeleccionar(TipoSangre tipoSangre) {
	  Seleccionado = tipoSangre;
	}
	
	public void actionListenerActualizar() {
		try {
			m.actualizarTipoSangre(Seleccionado);
			lista = m.findAllTipoSangre();
			JSFutil.crearMensajeInfo("Dato actualizado");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFutil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public List<TipoSangre> getLista() {
		return lista;
	}

	public void setLista(List<TipoSangre> lista) {
		this.lista = lista;
	}

	public TipoSangre getTipoSangre() {
		return tipoSangre;
	}

	public void setTipoSangre(TipoSangre tipoSangre) {
		this.tipoSangre = tipoSangre;
	}

	public boolean isPanelColapsado() {
		return panelColapsado;
	}

	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}

	public TipoSangre getSeleccionado() {
		return Seleccionado;
	}

	public void setSeleccionado(TipoSangre seleccionado) {
		Seleccionado = seleccionado;
	}
	
	


}
