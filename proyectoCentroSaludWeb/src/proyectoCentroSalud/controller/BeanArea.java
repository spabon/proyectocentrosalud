package proyectoCentroSalud.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import proyectoCentroSalud.model.entities.Area;
import proyectoCentroSalud.model.manager.ManagerArea;
import proyectoCentroSalud.view.util.JSFutil;

@Named
@SessionScoped
public class BeanArea implements Serializable{
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerArea mArea;
	private List<Area> listaArea;
	private Area area;
	private boolean panelColapsadoArea;
	private Area areaSeleccionado;
	
	@PostConstruct
	public void inicializar() {
		panelColapsadoArea = true;
		area = new Area();
		listaArea=mArea.findAllArea();		
	}
	
	public void actionListenerColapsarPanelArea() {
		panelColapsadoArea = !panelColapsadoArea;
	}

	public void actionListenerInsertarArea() {
		try {
			System.out.print(area.getDescripcion());
			mArea.insertarArea(area);
			listaArea = mArea.findAllArea();
			area = new Area();
			JSFutil.crearMensajeInfo("Dato ingresado");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFutil.crearMensajeError("Dato Duplicado");
			e.printStackTrace();
		}
	}
	
	public void actionListenerEliminarArea(int id_area) {
		mArea.eliminarArea(id_area);
		listaArea = mArea.findAllArea();
		JSFutil.crearMensajeInfo("Dato eliminado");
	}

	public void actionListenerSeleccionarArea(Area area) {
		areaSeleccionado = area;
	}
	
	public void actionListenerActualizarArea() {
		try {
			mArea.actualizarArea(areaSeleccionado);
			listaArea = mArea.findAllArea();
			JSFutil.crearMensajeInfo("Dato actualizado");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFutil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public List<Area> getListaArea() {
		return listaArea;
	}

	public void setListaArea(List<Area> listaArea) {
		this.listaArea = listaArea;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Area getAreaSeleccionado() {
		return areaSeleccionado;
	}

	public void setAreaSeleccionado(Area areaSeleccionado) {
		this.areaSeleccionado = areaSeleccionado;
	}

	public boolean isPanelColapsadoArea() {
		return panelColapsadoArea;
	}

	public void setPanelColapsadoArea(boolean panelColapsadoArea) {
		this.panelColapsadoArea = panelColapsadoArea;
	}

	



}
