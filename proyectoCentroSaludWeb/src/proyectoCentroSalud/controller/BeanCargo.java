package proyectoCentroSalud.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import proyectoCentroSalud.model.entities.Cargo;
import proyectoCentroSalud.model.manager.ManagerCargo;
import proyectoCentroSalud.view.util.JSFutil;

@Named
@SessionScoped
public class BeanCargo implements Serializable{
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerCargo m;
	private List<Cargo> listaArea;
	private Cargo cargo;
	private boolean panelColapsadoCargo;
	private Cargo cargoSeleccionado;
	
	
	@PostConstruct
	public void inicializar() {
		panelColapsadoCargo = true;
		cargo = new Cargo();
		listaArea=m.findAllCargo();		
	}
	
	public void actionListenerColapsarPanelCargo() {
		panelColapsadoCargo = !panelColapsadoCargo;
	}

	public void actionListenerInsertarCargo() {
		try {
			System.out.print(cargo.getEspecializacion());
			m.insertarCargo(cargo);
			listaArea = m.findAllCargo();
			cargo = new Cargo();
			JSFutil.crearMensajeInfo("Dato ingresado");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFutil.crearMensajeError("Dato Duplicado");
			e.printStackTrace();
		}
	}
	
	public void actionListenerEliminarCargo(int id_cargo) {
		m.eliminarCargo(id_cargo);;
		listaArea = m.findAllCargo();
		JSFutil.crearMensajeInfo("Dato eliminado");
	}

	public void actionListenerSeleccionarCargo(Cargo area) {
		cargoSeleccionado = area;
	}
	
	public void actionListenerActualizarCargo() {
		try {
			m.actualizarCargo(cargoSeleccionado);
			listaArea = m.findAllCargo();
			JSFutil.crearMensajeInfo("Dato actualizado");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFutil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public List<Cargo> getListaArea() {
		return listaArea;
	}

	public void setListaArea(List<Cargo> listaArea) {
		this.listaArea = listaArea;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public boolean isPanelColapsadoCargo() {
		return panelColapsadoCargo;
	}

	public void setPanelColapsadoCargo(boolean panelColapsadoCargo) {
		this.panelColapsadoCargo = panelColapsadoCargo;
	}

	public Cargo getCargoSeleccionado() {
		return cargoSeleccionado;
	}

	public void setCargoSeleccionado(Cargo cargoSeleccionado) {
		this.cargoSeleccionado = cargoSeleccionado;
	}
	
	

}
