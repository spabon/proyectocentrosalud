package proyectoCentroSalud.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;


import proyectoCentroSalud.model.entities.Ciudad;
import proyectoCentroSalud.model.entities.Genero;

import proyectoCentroSalud.model.entities.Paciente;
import proyectoCentroSalud.model.entities.TipoSangre;
import proyectoCentroSalud.model.manager.ManagerPaciente;

@Named
@SessionScoped
public class BeanPaciente implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ManagerPaciente mPaciente;
	private List<Paciente> lista_paciente;
	private List<TipoSangre> listaTiposangre;
	private List<Genero> listaGenero;
	private List<Ciudad> listaCiudad;
	

	private int tiposangre_i;
	private int genero_i;
	private int ciudad_i;
	private Paciente crearPaciente;
	private TipoSangre tiposangre;
	private Genero genero;
	private Ciudad ciudad;
	
	@PostConstruct
	public void init() {
		lista_paciente = mPaciente.findAllPaciente();
		listaTiposangre = mPaciente.findAllTipoSangre();
		listaGenero = mPaciente.findAllGenero();
		listaCiudad = mPaciente.findAllCiudad();
		
		crearPaciente = new Paciente();
		tiposangre = new TipoSangre();
		genero = new Genero();
		ciudad = new Ciudad();
	}
	
	public void actionListenerInsertarPaciente() {
		try {
			
			mPaciente.insertarPaciente( crearPaciente,genero_i, ciudad_i, tiposangre_i);
			lista_paciente = mPaciente.findAllPaciente();
			crearPaciente = new Paciente();
		}catch (Exception e) {
			e.getMessage();
			e.printStackTrace();
		}
	}

	public List<Paciente> getLista_paciente() {
		return lista_paciente;
	}

	public void setLista_paciente(List<Paciente> lista_paciente) {
		this.lista_paciente = lista_paciente;
	}

	public List<TipoSangre> getListaTiposangre() {
		return listaTiposangre;
	}

	public void setListaTiposangre(List<TipoSangre> listaTiposangre) {
		this.listaTiposangre = listaTiposangre;
	}

	public List<Genero> getListaGenero() {
		return listaGenero;
	}

	public void setListaGenero(List<Genero> listaGenero) {
		this.listaGenero = listaGenero;
	}

	public List<Ciudad> getListaCiudad() {
		return listaCiudad;
	}

	public void setListaCiudad(List<Ciudad> listaCiudad) {
		this.listaCiudad = listaCiudad;
	}

	public int getTiposangre_i() {
		return tiposangre_i;
	}

	public void setTiposangre_i(int tiposangre_i) {
		this.tiposangre_i = tiposangre_i;
	}

	public int getGenero_i() {
		return genero_i;
	}

	public void setGenero_i(int genero_i) {
		this.genero_i = genero_i;
	}

	public int getCiudad_i() {
		return ciudad_i;
	}

	public void setCiudad_i(int ciudad_i) {
		this.ciudad_i = ciudad_i;
	}

	public Paciente getCrearPaciente() {
		return crearPaciente;
	}

	public void setCrearPaciente(Paciente crearPaciente) {
		this.crearPaciente = crearPaciente;
	}

	public TipoSangre getTiposangre() {
		return tiposangre;
	}

	public void setTiposangre(TipoSangre tiposangre) {
		this.tiposangre = tiposangre;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public Ciudad getCiudad() {
		return ciudad;
	}

	public void setCiudad(Ciudad ciudad) {
		this.ciudad = ciudad;
	}

	
}
