package proyectoCentroSalud.controller;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import proyectoCentroSalud.model.entities.Paciente;
import proyectoCentroSalud.model.entities.Prueba;
import proyectoCentroSalud.model.manager.ManagerPrueba;
import proyectoCentroSalud.view.util.JSFutil;

@Named
@SessionScoped
public class BeanPrueba implements Serializable{
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerPrueba mPrueba;
	private List<Prueba> listaPrueba;
	private Prueba prueba;
	private boolean panelColapsadoPrueba;
	private Prueba pruebaSeleccionada;
	private String description;
	private Date fecha;
	private int cantidad;
	private double precio;
	private int pago;	
	private double pago1;	
	
	
	
	@PostConstruct
	public void inicializar() {
		panelColapsadoPrueba = true;
		prueba = new Prueba();
		listaPrueba = mPrueba.findAllPrueba();
		pago=mPrueba.totalS(listaPrueba);
	}
	
	public void actionListenerInsertarPrueba() {
		try {
		mPrueba.insertarPrueba(prueba);
	
	listaPrueba = mPrueba.findAllPrueba();
	pruebaSeleccionada = new Prueba();
	pago=mPrueba.totalS(listaPrueba);
//	pago1=mPrueba.totalSumaP(listaPrueba);
	}catch (Exception e) {
		e.getMessage();
		e.printStackTrace();
	}
}
	
	
	
	public void actionListenerEliminarPrueba(int idprueba) {
		mPrueba.eliminarPrueba(idprueba);
		listaPrueba=mPrueba.findAllPrueba(); 
		pago=mPrueba.totalS(listaPrueba);
		JSFutil.crearMensajeInfo("Prueba eliminado");
	}
	

	//public double actionListenerSumaPrueba () {
		//pago = mPrueba.Sumar(List<Prueba> prueba);
	//	return pago;
	//}

	
	
	public int getPago() {
		return pago;
	}

	public void setPago(int pago) {
		this.pago = pago;
	}

	public double getPago1() {
		return pago1;
	}

	public void setPago1(double pago1) {
		this.pago1 = pago1;
	}

	public ManagerPrueba getmPrueba() {
		return mPrueba;
	}

	public void setmPrueba(ManagerPrueba mPrueba) {
		this.mPrueba = mPrueba;
	}

	public List<Prueba> getListaPrueba() {
		return listaPrueba;
	}

	public void setListaPrueba(List<Prueba> listaPrueba) {
		this.listaPrueba = listaPrueba;
	}

	public Prueba getPrueba() {
		return prueba;
	}

	public void setPrueba(Prueba prueba) {
		this.prueba = prueba;
	}

	public boolean isPanelColapsadoPrueba() {
		return panelColapsadoPrueba;
	}

	public void setPanelColapsadoPrueba(boolean panelColapsadoPrueba) {
		this.panelColapsadoPrueba = panelColapsadoPrueba;
	}

	public Prueba getPruebaSeleccionada() {
		return pruebaSeleccionada;
	}

	public void setPruebaSeleccionada(Prueba pruebaSeleccionada) {
		this.pruebaSeleccionada = pruebaSeleccionada;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
}