package proyectoCentroSalud.controller;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import proyectoCentroSalud.model.entities.Cargo;
import proyectoCentroSalud.model.entities.Ciudad;
import proyectoCentroSalud.model.entities.Genero;
import proyectoCentroSalud.model.entities.Medico;
import proyectoCentroSalud.model.manager.ManagerMedico;

import java.io.Serializable;
import java.util.List;


@Named
@SessionScoped
public class BeanMedico implements Serializable {
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerMedico mMedico;
	private List<Medico> listaMedicos;
	private List<Cargo> listaCargo;
	private List<Genero> listaGenero;
	private List<Ciudad> listaCiudad;
	
	private int cargo_i;
	private int genero_i;
	private int ciudad_i;
	private Medico crearMedico;
	private Cargo cargo;
	private Genero genero;
	private Ciudad ciudad;
	
	@PostConstruct
	public void init() {
		listaMedicos = mMedico.findAllMedico();
		listaCargo = mMedico.findAllCargo();
		listaGenero = mMedico.findAllGenero();
		listaCiudad = mMedico.findAllCiudad();
		
		crearMedico = new Medico();
		cargo = new Cargo();
		genero = new Genero();
		ciudad = new Ciudad();
	}
	
	public void actionListenerInsertarMedico() {
		try {
			mMedico.insertarMedico(crearMedico, cargo_i, genero_i, ciudad_i);
			listaMedicos = mMedico.findAllMedico();
			crearMedico = new Medico();
		}catch (Exception e) {
			e.getMessage();
			e.printStackTrace();
		}
	}

	public List<Medico> getListaMedicos() {
		return listaMedicos;
	}

	public void setListaMedicos(List<Medico> listaMedicos) {
		this.listaMedicos = listaMedicos;
	}

	public List<Cargo> getListaCargo() {
		return listaCargo;
	}

	public void setListaCargo(List<Cargo> listaCargo) {
		this.listaCargo = listaCargo;
	}

	public List<Genero> getListaGenero() {
		return listaGenero;
	}

	public void setListaGenero(List<Genero> listaGenero) {
		this.listaGenero = listaGenero;
	}

	public List<Ciudad> getListaCiudad() {
		return listaCiudad;
	}

	public void setListaCiudad(List<Ciudad> listaCiudad) {
		this.listaCiudad = listaCiudad;
	}

	public int getCargo_i() {
		return cargo_i;
	}

	public void setCargo_i(int cargo_i) {
		this.cargo_i = cargo_i;
	}

	public int getGenero_i() {
		return genero_i;
	}

	public void setGenero_i(int genero_i) {
		this.genero_i = genero_i;
	}

	public int getCiudad_i() {
		return ciudad_i;
	}

	public void setCiudad_i(int ciudad_i) {
		this.ciudad_i = ciudad_i;
	}

	public Medico getCrearMedico() {
		return crearMedico;
	}

	public void setCrearMedico(Medico crearMedico) {
		this.crearMedico = crearMedico;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public Ciudad getCiudad() {
		return ciudad;
	}

	public void setCiudad(Ciudad ciudad) {
		this.ciudad = ciudad;
	}
	
	
	
}
