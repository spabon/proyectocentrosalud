package proyectoCentroSalud.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import proyectoCentroSalud.model.entities.Ciudad;
import proyectoCentroSalud.model.manager.ManagerCiudad;
import proyectoCentroSalud.view.util.JSFutil;

@Named
@SessionScoped
public class BeanCiudad implements Serializable{
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerCiudad m;
	private List<Ciudad> lista;
	private Ciudad ciudad;
	private boolean panelColapsadoCiudad;
	private Ciudad ciudadSeleccionado;
	
	@PostConstruct
	public void inicializar() {
		panelColapsadoCiudad = true;
		ciudad = new Ciudad();
		lista=m.findAllCiudad();		
	}
	
	public void actionListenerColapsarPanelCiudad() {
		panelColapsadoCiudad = !panelColapsadoCiudad;
	}

	public void actionListenerInsertarCiudad() {
		try {
			System.out.print(ciudad.getNombre());
			m.insertarCiudad(ciudad);
			lista = m.findAllCiudad();
			ciudad = new Ciudad();
			JSFutil.crearMensajeInfo("Dato ingresado");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFutil.crearMensajeError("Dato Duplicado");
			e.printStackTrace();
		}
	}
	
	public void actionListenerEliminarCiudad(int id_ciudad) {
		m.eliminarCiudad(id_ciudad);;
		lista = m.findAllCiudad();
		JSFutil.crearMensajeInfo("Dato eliminado");
	}

	public void actionListenerSeleccionarCiudad(Ciudad ciudad) {
		ciudadSeleccionado = ciudad;
	}
	
	public void actionListenerActualizarCiudad() {
		try {
			m.actualizarCiudad(ciudadSeleccionado);
			lista = m.findAllCiudad();
			JSFutil.crearMensajeInfo("Dato actualizado");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFutil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public List<Ciudad> getLista() {
		return lista;
	}

	public void setLista(List<Ciudad> lista) {
		this.lista = lista;
	}

	public Ciudad getCiudad() {
		return ciudad;
	}

	public void setCiudad(Ciudad ciudad) {
		this.ciudad = ciudad;
	}

	public boolean isPanelColapsadoCiudad() {
		return panelColapsadoCiudad;
	}

	public void setPanelColapsadoCiudad(boolean panelColapsadoCiudad) {
		this.panelColapsadoCiudad = panelColapsadoCiudad;
	}

	public Ciudad getCiudadSeleccionado() {
		return ciudadSeleccionado;
	}

	public void setCiudadSeleccionado(Ciudad ciudadSeleccionado) {
		this.ciudadSeleccionado = ciudadSeleccionado;
	}
	
	
	
}