package proyectoCentroSalud.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import proyectoCentroSalud.model.entities.Genero;
import proyectoCentroSalud.model.manager.ManagerGenero;
import proyectoCentroSalud.view.util.JSFutil;

@Named
@SessionScoped
public class BeanGenero implements Serializable{
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerGenero m;
	private List<Genero> lista;
	private Genero genero;
	private boolean panelColapsado;
	private Genero Seleccionado;
	
	@PostConstruct
	public void inicializar() {
		panelColapsado = true;
		genero = new Genero();
		lista=m.findAll();		
	}
	
	public void actionListenerColapsarPanelGenero() {
		panelColapsado = !panelColapsado;
	}

	public void actionListenerInsertar() {
		try {
			System.out.print(genero.getNombre());
			m.insertar(genero);
			lista = m.findAll();
			genero = new Genero();
			JSFutil.crearMensajeInfo("Dato ingresado");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFutil.crearMensajeError("Dato Duplicado");
			e.printStackTrace();
		}
	}
	
	public void actionListenerEliminar(int id_genero) {
		m.eliminar(id_genero);
		lista = m.findAll();
		JSFutil.crearMensajeInfo("Dato eliminado");
	}

	public void actionListenerSeleccionar(Genero genero) {
	  Seleccionado = genero;
	}
	
	public void actionListenerActualizar() {
		try {
			m.actualizar(Seleccionado);
			lista = m.findAll();
			JSFutil.crearMensajeInfo("Dato actualizado");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFutil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public List<Genero> getLista() {
		return lista;
	}

	public void setLista(List<Genero> lista) {
		this.lista = lista;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public boolean isPanelColapsado() {
		return panelColapsado;
	}

	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}

	public Genero getSeleccionado() {
		return Seleccionado;
	}

	public void setSeleccionado(Genero seleccionado) {
		Seleccionado = seleccionado;
	}
	
	

}
