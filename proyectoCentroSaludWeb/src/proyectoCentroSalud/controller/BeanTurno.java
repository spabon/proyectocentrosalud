package proyectoCentroSalud.controller;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import proyectoCentroSalud.model.entities.Area;
import proyectoCentroSalud.model.entities.Medico;
import proyectoCentroSalud.model.entities.Paciente;
import proyectoCentroSalud.model.entities.Turno;
import proyectoCentroSalud.model.manager.ManagerTurno;

import java.io.Serializable;
import java.util.List;


@Named
@SessionScoped
public class BeanTurno implements Serializable {

	private static final long serialVersionUID = 1L;

	
	@EJB
	private ManagerTurno mTurno;
	private List<Turno> lista_turno;
	private List<Paciente> lista_paciente;
	private List<Medico> listaMedicos;
	private List<Area> listaArea;
	

	private int medico_i;
	private int paciente_i;
	private int area_i;
	private Turno crearTurno;
	private Paciente paciente;
	private Medico medico;
	private Area area;
	
	@PostConstruct
	public void init() {
		lista_turno = mTurno.findAllTurno();
		listaMedicos = mTurno.findAllMedico();
		listaArea = mTurno.findAllArea();
		lista_paciente = mTurno.findAllPaciente();
		
		crearTurno = new Turno();
		medico = new Medico();
		paciente= new Paciente();
		area = new Area();
	}
	
	public void actionListenerInsertarTurno() {
		try {
			mTurno.insertarTurno( crearTurno,medico_i, paciente_i, area_i);
			lista_turno = mTurno.findAllTurno();
			crearTurno = new Turno();
		}catch (Exception e) {
			e.getMessage();
			e.printStackTrace();
		}
	}

	public List<Turno> getLista_turno() {
		return lista_turno;
	}

	public void setLista_turno(List<Turno> lista_turno) {
		this.lista_turno = lista_turno;
	}

	public List<Paciente> getLista_paciente() {
		return lista_paciente;
	}

	public void setLista_paciente(List<Paciente> lista_paciente) {
		this.lista_paciente = lista_paciente;
	}

	public List<Medico> getListaMedicos() {
		return listaMedicos;
	}

	public void setListaMedicos(List<Medico> listaMedicos) {
		this.listaMedicos = listaMedicos;
	}

	public List<Area> getListaArea() {
		return listaArea;
	}

	public void setListaArea(List<Area> listaArea) {
		this.listaArea = listaArea;
	}

	public int getMedico_i() {
		return medico_i;
	}

	public void setMedico_i(int medico_i) {
		this.medico_i = medico_i;
	}

	public int getPaciente_i() {
		return paciente_i;
	}

	public void setPaciente_i(int paciente_i) {
		this.paciente_i = paciente_i;
	}

	public int getArea_i() {
		return area_i;
	}

	public void setArea_i(int area_i) {
		this.area_i = area_i;
	}

	public Turno getCrearTurno() {
		return crearTurno;
	}

	public void setCrearTurno(Turno crearTurno) {
		this.crearTurno = crearTurno;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public Medico getMedico() {
		return medico;
	}

	public void setMedico(Medico medico) {
		this.medico = medico;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}
	
	

	

}
