package proyectoCentroSalud.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import proyectoCentroSalud.model.entities.Suple;
import proyectoCentroSalud.model.manager.ManagerSuple;
import proyectoCentroSalud.view.util.JSFutil;

@Named
@SessionScoped
public class BeanSuple implements Serializable {
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerSuple mSuple;
	private List<Suple> listaSuple;
	private Suple suple;
	private  boolean panelColapsadoSuple;
	private Suple supleSeleccionado;
	
	@PostConstruct
	public void inicializar() {
		panelColapsadoSuple = true;
		suple = new Suple();
		listaSuple=mSuple.findAllSuple();
	}
	
	public void actionListarColapsarPanelSuple() {
		panelColapsadoSuple = !panelColapsadoSuple;
	}
	
	public void actionListenerInsertarSuple() {
		try {
			System.out.print(suple.getDescripcion());
			mSuple.insertarSuple(suple);
			listaSuple = mSuple.findAllSuple();
			suple = new Suple();
			JSFutil.crearMensajeInfo("Dato ingresado");
		}catch (Exception e) {
			// TODO: handle exception
			JSFutil.crearMensajeError("Datos Duplicado");
			e.printStackTrace();
		}

	}
	
	public void actionListenerEliminarSuple(int id_suple) {
		mSuple.eliminarSuple(id_suple);
		listaSuple = mSuple.findAllSuple();
		JSFutil.crearMensajeInfo("Datos eliminados");
	}
	
	public void actionListenerSeleccionarSuple(Suple suple) {
		supleSeleccionado = suple;
	}
	

	public void actionListenerActualizarSuple() {
		try {
			mSuple.actualizarSuple(supleSeleccionado);
			listaSuple = mSuple.findAllSuple();
			JSFutil.crearMensajeInfo("Dato actualizado");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFutil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public List<Suple> getListaSuple() {
		return listaSuple;
	}

	public void setListaSuple(List<Suple> listaSuple) {
		this.listaSuple = listaSuple;
	}

	public Suple getSuple() {
		return suple;
	}

	public void setSuple(Suple suple) {
		this.suple = suple;
	}

	public boolean isPanelColapsadoSuple() {
		return panelColapsadoSuple;
	}

	public void setPanelColapsadoSuple(boolean panelColapsadoSuple) {
		this.panelColapsadoSuple = panelColapsadoSuple;
	}

	public Suple getSupleSeleccionado() {
		return supleSeleccionado;
	}

	public void setSupleSeleccionado(Suple supleSeleccionado) {
		this.supleSeleccionado = supleSeleccionado;
	}
	
	
	
	
}

	
	
	
	

	
